#Author and copyrights to: Papas Ioannis 9218
#Operational research project 2020

set I:={2..9};
set J:={1..9};

#Initialized at the .dat file
param neededA {i in J} integer;
param neededB {i in J} integer;

#Number of products producted every week
var A {i in I} integer>=0;
var B {i in I} integer>=0;

#Which of the two products is producted every week
#1 is for A, 0 is for B
var y {i in J} binary;

#Number of left products at the end of each week
var storageA {i in J} integer>=0;
var storageB {i in J} integer>=0;

#Check if the production changed
#1 if it changed, 0 if not
var changed {i in I} binary;

#The objective
minimize Total_cost: sum {i in I} (A[i]*225+B[i]*310+changed[i]*500)
					+sum {j in J} ((storageA[j]*225+storageB[j]*310)*(0.195/52));

#Initial conditions
subject to initialStorageA: storageA[1]=125;
subject to initialStorageB: storageB[1]=143;
subject to initialworking: y[1]=1;

#Maximum rate of production
subject to maxProductionAConstraint {i in I}:
	A[i]<=100;
	
subject to maxProductionBConstraint {i in I}:
	B[i]<=80;	

#Constraint that only one of the productions works
subject to whichWorks1 {i in I}:
	A[i]<=y[i]*5000;
	
subject to whichWorks2 {i in I}:
	B[i]<=(1-y[i])*5000;	

#Calculate the new left products at the end of the week
subject to storageAConstraints {i in I}:
	storageA[i]=storageA[i-1]+A[i]-neededA[i-1];
	
subject to storageBConstraints {i in I}:
	storageB[i]=storageB[i-1]+B[i]-neededB[i-1];

#Left products at the end of one week must be more than the 80% of the products needed for the next week
subject to productionAConstraint {i in I}:
	neededA[i-1]+0.8*neededA[i]-storageA[i-1]<=A[i];
	
subject to productionBConstraint {i in I}:
	neededB[i-1]+0.8*neededB[i]-storageB[i-1]<=B[i];

#Check if changes or not
subject to ifChange1 {i in I}:
	changed[i]>=y[i]-y[i-1];
	
subject to ifChange2 {i in I}:
	changed[i]>=y[i-1]-y[i];
	

